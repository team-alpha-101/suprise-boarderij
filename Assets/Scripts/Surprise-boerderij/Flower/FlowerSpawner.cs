﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


//nino
/// <summary>
/// dit script spawned bliemen in binnen de navmesh op een timer.
/// </summary>
public class FlowerSpawner : MonoBehaviour
{
    [SerializeField]
    private int _radius;
    [SerializeField]
    private GameObject _prefab;
    [SerializeField]
    private Timer _timer;
    
    public float TimerFlower;
    // Start is called before the first frame update
    void Start()
    {
        TimerFlower =_timer.flowerTimer;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (TimerFlower >= 0.0f)
        {
            TimerFlower -= Time.deltaTime;
        }
 
        if (TimerFlower <= 0.0f)
        {
            Vector3 randomDirection = Random.insideUnitSphere * _radius; 
            randomDirection += transform.position; 
            NavMeshHit hit; 
            NavMesh.SamplePosition(randomDirection, out hit, _radius, 1);

            Vector3 finalPosition = hit.position;
        
            Instantiate(_prefab, finalPosition, Quaternion.identity); 
            
            timerEnded();
        }
     
          
    }
    
    private void timerEnded()
    {
        _timer.flowerTimer = 10;
        TimerFlower =_timer.flowerTimer;
    }
    

    

    
   
   
   
}
