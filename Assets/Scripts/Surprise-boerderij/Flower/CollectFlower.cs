﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//nino
/// <summary>
/// kort script wat op de button van de flower gaat om de sterren te verzamelen.
/// dit word gaarna niet actief gemaakt zodat je niet meer sterren kunt verzamelen terwijl de bloem nog leeft.
/// </summary>
public class CollectFlower : MonoBehaviour
{
 

    void OnMouseDown()
    {
        ResourceData.Instance.starPoints += 2;
        EventManager.Instance.ResourceUpdate();
        gameObject.SetActive(false);
     
    }
}
