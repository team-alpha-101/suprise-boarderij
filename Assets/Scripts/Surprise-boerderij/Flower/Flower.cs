﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//nino
/// <summary>
/// dit script houd bij hoelang de bloem nog leeft en zorgt dat in de laatste 10 seconden de bloemv erzameld kan worden.
/// </summary>

public class Flower : MonoBehaviour
{
    [SerializeField]
    private Timer _timer;
    [SerializeField]
    private GameObject _button;

    private bool _isNotActive = true;
    
    public float FlowerLifeTime;
    
    // Start is called before the first frame update
    void Start()
    {
        FlowerLifeTime =_timer.flowerLifeTime;
        _button.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (FlowerLifeTime >= 0.0f)
        {
            FlowerLifeTime -= Time.deltaTime;
           
        }
        
        if (FlowerLifeTime <= 10.0f && FlowerLifeTime > 0.0f && _isNotActive)
        {
            _button.gameObject.SetActive(true);
            _isNotActive = false;
        }
 
        if (FlowerLifeTime <= 0.0f)
        {
            timerEnded();
        }
        
    }
    private void timerEnded()
    {
        _timer.flowerTimer = 60;
        Destroy(gameObject);
    }
    
    
}
