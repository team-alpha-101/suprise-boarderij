﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// Dit script zogt voor de interacties van monster zoals eten en water geven.
/// </summary>

public class MonsterInteractions : MonoBehaviour
{
    [SerializeField]
    private MonsterType.monsterType _monsterType;

    [SerializeField]
    private ParticleSystem _heartFood;

    [SerializeField]
    private ParticleSystem _heartWater;

    [SerializeField]
    private float _needsDecrease;

    [SerializeField]
    private Monster monster;


    private void Awake()
    {
        EventManager.onFeedMonster += Feed;
        EventManager.onGiveWater += Drink;
    }

    private void OnDestroy()
    {
        EventManager.onFeedMonster -= Feed;
        EventManager.onGiveWater -= Drink;
    }

    private void Feed(MonsterType.monsterType monsterType)
    {
        if (ResourceData.Instance.food <= 9 || monsterType != _monsterType) return;
        monster.currentHunger -= _needsDecrease;
        ResourceData.Instance.food -= 10;
        GainPoints();
        ResourceData.Instance.SaveData();
        _heartFood.Play();
        monster._foodButton.SetActive(false);
    }

    private void Drink(MonsterType.monsterType monsterType)
    {
        if (ResourceData.Instance.water <= 9 || monsterType != _monsterType) return;
        monster.currentThirst -= _needsDecrease;
        ResourceData.Instance.water -= 10;
        GainPoints();
        ResourceData.Instance.SaveData();
        _heartWater.Play();
        monster._waterButton.SetActive(false);
    }

    public void GainPoints()
    {
        ResourceData.Instance.starPoints += 10;
    }
}
