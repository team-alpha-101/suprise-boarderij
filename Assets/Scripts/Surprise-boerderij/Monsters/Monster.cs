﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// Dit sdript zogt er voor dat het monstertje honger en dorst krijgt.
/// </summary>

public class Monster : MonoBehaviour
{
    [SerializeField] public GameObject _foodButton;
    [SerializeField] public GameObject _waterButton;

    private float _currentHunger;
    private float _currentThirst;

    public float currentHunger { get { return _currentHunger; } set { _currentHunger = value; } }
    public float currentThirst { get { return _currentThirst; } set { _currentThirst = value; } }

    [SerializeField]
    private float _needsIncrease;

    private float _foodTimer = 3;
    private float _waterTimer = 3;

    [SerializeField]
    private float _timeReset;

    // Timers for food and water with limit check on hunger & thirst

    public void Update()
    {
        _foodTimer -= Time.deltaTime;
        _waterTimer -= Time.deltaTime;
       
        if (_foodTimer <= 0 && currentHunger < 100)
        {
                GainHunger();
                _foodTimer = _timeReset;
        }
        
        if (_waterTimer <= 0 && currentThirst < 100)
        {
                GainThirst();
                _waterTimer = _timeReset;
        }
    }

    private void GainHunger()
    {
        _currentHunger += _needsIncrease;

        if (_currentHunger >= 50)
        {
            _foodButton.SetActive(true);
        }
    }

    private void GainThirst()
    {
        _currentThirst += _needsIncrease;
        
        if (_currentThirst >= 50)
        { 
            _waterButton.SetActive(true);
        }
    }
}
