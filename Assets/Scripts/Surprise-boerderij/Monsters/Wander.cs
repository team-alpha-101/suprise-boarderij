﻿using UnityEngine;
using System.Collections;

//sebastiaan
/// <summary>
/// Dit script regelt de random movement van het monstertje op de navmesh.
/// dit houd rekening met alle obstakels.
/// </summary>
public class Wander : MonoBehaviour {
 [SerializeField]
    private float wanderRadius;
    [SerializeField]
    private float wanderTimer;
    private Transform _target;
    private UnityEngine.AI.NavMeshAgent _agent;
    private float _timer;
 
    void OnEnable () 
    {
        _agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
        _timer = wanderTimer;
    }
 
    void Update () 
    {
        _timer += Time.deltaTime;

        if (!(_timer >= wanderTimer)) return;
        var newPos = RandomNavSphere(transform.position, wanderRadius, -1);
        _agent.SetDestination(newPos);
        _timer = 0;
    }

    private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layerMask) 
    {
        var randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        UnityEngine.AI.NavMeshHit navHit;
 
        UnityEngine.AI.NavMesh.SamplePosition (randDirection, out navHit, dist, layerMask);
 
        return navHit.position;
    }
}