﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR;
using UnityEngine.XR.ARSubsystems;
using System;

//nino
 
/// <summary>
/// dit script werk samen met de arsession orgin en de ar pose om zo een object op echte objecten neer te kunnen zetten
/// door de pose word hier rekening gehouden met de richting en rotatie van de telefoon 
/// zodra het object neergezet is zal dit object op niet actief worden gezet dit kan later weer enabled worden om nog een field in te spawnen.
/// </summary>

public class PlacePlayField : MonoBehaviour
{
    [SerializeField]
    private GameObject _objectToPlace;
    [SerializeField]
    private GameObject _placementIndicator;
    private Camera _myCamera;
    private ARSessionOrigin _arOrigin;
    private ARRaycastManager _raycastManager;
    private Pose _placementPose;
    private bool _placementPoseIsValid = false;
    

    void Start()
    {
        _arOrigin = FindObjectOfType<ARSessionOrigin>();
        _raycastManager = FindObjectOfType<ARRaycastManager>();
    
    }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        if (_placementPoseIsValid &&  Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            PlaceObject();
        }
    }

    private void PlaceObject()
    {
        Instantiate(_objectToPlace, _placementPose.position, _placementPose.rotation);
        
        gameObject.SetActive(false);
    }

    private void UpdatePlacementIndicator()
    {
        if (_placementPoseIsValid)
        {
            _placementIndicator.SetActive(true);
            _placementIndicator.transform.SetPositionAndRotation(_placementPose.position, _placementPose.rotation);
        }
        else
        {
            _placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        _raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        _placementPoseIsValid = hits.Count > 0;
        if (!_placementPoseIsValid) return;
        _placementPose = hits[0].pose;

        var cameraForward = Camera.current.transform.forward;
        var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
        _placementPose.rotation = Quaternion.LookRotation(cameraBearing);
    }

}