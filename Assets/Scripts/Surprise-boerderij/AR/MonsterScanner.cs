﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//nino

/// <summary>
/// dit is een simpel script die kijkt of 1 van de rtacked images overeen komt
/// met wat er verwacht word als dat gebeurt worden de monstertjes
/// op actief gezet.
/// </summary>

namespace AR
{
    [RequireComponent(typeof(ARTrackedImageManager))]
    public class MonsterScanner : MonoBehaviour
    {
        [SerializeField]
        private GameObject M1;
        [SerializeField]
        private GameObject M2;
    
        ARTrackedImageManager m_TrackedImageManager;

        void Awake()
        {
            m_TrackedImageManager = GetComponent<ARTrackedImageManager>();
        }

        void OnEnable()
        {
            m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
        }

        void OnDisable()
        {
            m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
        }

        void UpdateInfo(ARTrackedImage trackedImage)
        {
            if (trackedImage.name == "monster1")
                M1.SetActive(true);
            else if (trackedImage.name == "monster2") M2.SetActive(true);

        }

        void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            foreach (var trackedImage in eventArgs.added)
            {
                UpdateInfo(trackedImage);
            }

            foreach (var trackedImage in eventArgs.updated)
            {
                UpdateInfo(trackedImage);
            }
           
        }
    }
}
