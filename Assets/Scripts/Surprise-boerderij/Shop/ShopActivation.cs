﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// dit activeerd de shop
/// </summary>
/// 
public class ShopActivation : MonoBehaviour
{
    [SerializeField]
    private GameObject _shopCanvas;

    private void Awake()
    {
        _shopCanvas.SetActive(false);
    }

    private void OnMouseDown()
    {
        _shopCanvas.SetActive(true);
    }

    public void CloseShop()
    {
        _shopCanvas.SetActive(false);
    }
}
