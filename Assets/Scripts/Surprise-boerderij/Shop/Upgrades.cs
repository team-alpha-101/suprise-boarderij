﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

// Tyrone
/// <summary>
/// dit zijn alle upgrades en de data die daarbij hoort.
/// </summary>

public class Upgrades : MonoBehaviour
{
    private int _cost1 = 100; // Water Storage cost
    private int _cost2 = 100; // Food Storage cost
    private int _cost3 = 100; // Berry Field cost
    private int _cost4 = 100; // Water Pump cost

    public FoodCollectButton foodCollectButton;
    public WaterCollectButton waterCollectButton;

    [SerializeField]
    private Button _upgradeWater;
    [SerializeField]
    private Button _upgradeFood;
    [SerializeField]
    private Button _upgradeBerry;
    [SerializeField]
    private Button _upgradePump;

    [SerializeField]
    private Image _upgradeIcon1;
    [SerializeField]
    private Image _upgradeIcon2;
    [SerializeField]
    private Image _upgradeIcon3;
    [SerializeField]
    private Image _upgradeIcon4;


    [SerializeField]
    private Text _cost1Text;
    [SerializeField]
    private Text _cost2Text;
    [SerializeField]
    private Text _cost3Text;
    [SerializeField]
    private Text _cost4Text;

    public void Start()
    {
        GetCostData();

        _cost1Text.text = _cost1.ToString();
        _cost2Text.text = _cost2.ToString();
        _cost3Text.text = _cost3.ToString();
        _cost4Text.text = _cost4.ToString();

        WaterUpgradeCheck();
        FoodUpgradeCheck();
        BerryUpgradeCheck();
        PumpUpgradecheck();
    }

    public void WaterUpgradeCheck()
    {
        if (_cost1 >= 500)
        {
            _upgradeWater.interactable = false;
            _cost1Text.text = "Max";
            _upgradeIcon1.enabled = false;
        }
        else
        {
            UpgradeWaterStorage();
        }
    }

    public void FoodUpgradeCheck()
    {
        if(_cost2 >= 500)
        {
            _upgradeFood.interactable = false;
            _cost2Text.text = "Max";
            _upgradeIcon2.enabled = false;
        }
        else
        {
            UpgradeFoodStorage();
        }
    }

    public void BerryUpgradeCheck()
    {

        if(_cost3 >= 500)
        {
            _upgradeBerry.interactable = false;
            _cost3Text.text = "Max";
            _upgradeIcon3.enabled = false;
        }
        else
        {
            UpgradeBerryField();
        }
    }

    public void PumpUpgradecheck()
    {
        if(_cost4 >= 500)
        {
            _upgradePump.interactable = false;
            _cost4Text.text = "Max";
            _upgradeIcon4.enabled = false;
        }
        else
        {
            UpgradeWaterPump();
        }
    }

    private void UpgradeWaterStorage()
    {
        if (ResourceData.Instance.starPoints >= _cost1)
        {
            ResourceData.Instance.starPoints -= _cost1;
            _cost1 += 100;
            ResourceData.Instance.maxWater += 50;
            ResourceData.Instance.SaveData();
            EventManager.Instance.ResourceUpdate();
            SaveCostData();
            _cost1Text.text = _cost1.ToString();
        }
    }

    private void UpgradeFoodStorage()
    {
        
        if(ResourceData.Instance.starPoints >= _cost2)
        {
            ResourceData.Instance.starPoints -= _cost2;
            _cost2 += 100;
            ResourceData.Instance.maxFood += 50;
            ResourceData.Instance.SaveData();
            EventManager.Instance.ResourceUpdate();
            SaveCostData();
            _cost2Text.text = _cost2.ToString();
        }
    }

    private void UpgradeBerryField()
    {
        if (ResourceData.Instance.starPoints >= _cost3)
        {
            ResourceData.Instance.starPoints -= _cost3;
            _cost3 += 100;
            foodCollectButton.foodAmount += 10;
            ResourceData.Instance.SaveData();
            EventManager.Instance.ResourceUpdate();
            SaveCostData();
            _cost3Text.text = _cost3.ToString();
        }
    }

    private void UpgradeWaterPump()
    {
        if (ResourceData.Instance.starPoints >= _cost4)
        {
            ResourceData.Instance.starPoints -= _cost4;
            _cost4 += 100;
            waterCollectButton.waterAmount += 10;
            ResourceData.Instance.SaveData();
            EventManager.Instance.ResourceUpdate();
            SaveCostData();
            _cost4Text.text = _cost4.ToString();
        }
    }

    private void GetCostData()
    {
        _cost1 = PlayerPrefs.GetInt("Cost1");
        _cost2 = PlayerPrefs.GetInt("Cost2");
        _cost3 = PlayerPrefs.GetInt("Cost3");
        _cost4 = PlayerPrefs.GetInt("Cost4");
        
    }

    public void SaveCostData()
    {
        PlayerPrefs.SetInt("Cost1", _cost1);
        PlayerPrefs.SetInt("Cost2", _cost2);
        PlayerPrefs.SetInt("Cost3", _cost3);
        PlayerPrefs.SetInt("Cost4", _cost4);
    }
}
