﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone

/// <summary>
/// Dit zijn alle events van het project.
/// </summary>

public class EventManager : MonoBehaviour
{
    public static EventManager Instance { get; private set; }

    public delegate void OnResourceUpdate();
    public static event OnResourceUpdate onResourceUpdate;

    public delegate void OnFeedMonster(MonsterType.monsterType monsterType);
    public static event OnFeedMonster onFeedMonster;

    public delegate void OnGiveWater(MonsterType.monsterType monsterType);
    public static event OnGiveWater onGiveWater;

    public delegate void OnUnlockMonster2();
    public static event OnUnlockMonster2 onUnlockMonster2;

    public delegate void OnUnlockMonster3();
    public static event OnUnlockMonster3 onUnlockMonster3;

    public delegate void OnUnlockMonster4();
    public static event OnUnlockMonster4 onUnlockMonster4;

    public delegate void OnUnlockMonster5();
    public static event OnUnlockMonster5 onUnlockMonster5;


    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ResourceUpdate()
    {
        if (onResourceUpdate != null)
        {
            onResourceUpdate();
        }
    }

    public void FeedMonster(MonsterType.monsterType monsterType)
    {
        if (onFeedMonster != null)
        {
            onFeedMonster(monsterType);
            
        }
    }

    public void GiveWater(MonsterType.monsterType monsterType)
    {
        if (onGiveWater != null)
        {
            onGiveWater(monsterType);
        }
    }
    
    public void UnlockMonster2()
    {
        if(onUnlockMonster2 != null)
        {
            onUnlockMonster2();
        }
    }

    public void UnlockMonster3()
    {
        if(onUnlockMonster3 != null)
        {
            onUnlockMonster3();
        }
    }

    public void UnlockMonster4()
    {
        if(onUnlockMonster4 != null)
        {
            onUnlockMonster4();
        }
    }

    public void UnlockMonster5()
    {
        if (onUnlockMonster5 != null)
        {
            onUnlockMonster5();
        }
    }
}
