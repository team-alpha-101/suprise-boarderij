﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone

public class EncyclopediaActivation : MonoBehaviour
{
    [SerializeField]
    private GameObject _encyclopediaCanvas;

    public void Awake()
    {
        _encyclopediaCanvas.SetActive(false);
    }

    public void OpenEncyclopedia()
    {
        _encyclopediaCanvas.SetActive(true);
    }

    public void CloseEncyclopedia()
    {
        _encyclopediaCanvas.SetActive(false);
    }
}
