﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// dit script regelt de unlocks van de encyclopedia.
/// </summary>

public class Encyclopedia : MonoBehaviour
{
    [SerializeField]
    private Canvas _encylopediaCanvas;

    [SerializeField]
    private Image _monster1Cover;
    [SerializeField]
    private Image _monster2Cover;
    [SerializeField]
    private Image _monster3Cover;
    [SerializeField]
    private Image _monster4Cover;


    [SerializeField]
    private Text _monster1Text;
    [SerializeField]
    private Text _monster2Text;
    [SerializeField]
    private Text _monster3Text;
    [SerializeField]
    private Text _monster4Text;


    private void Awake()
    {
        _monster2Cover.enabled = true;
        _monster3Cover.enabled = true;
        _monster4Cover.enabled = true;

        EventManager.onUnlockMonster2 += UnlockMonster2;
        EventManager.onUnlockMonster3 += UnlockMonster3;
        EventManager.onUnlockMonster4 += UnlockMonster4;
    }

    private void OnDestroy()
    {
        EventManager.onUnlockMonster2 -= UnlockMonster2;
        EventManager.onUnlockMonster3 -= UnlockMonster3;
        EventManager.onUnlockMonster4 -= UnlockMonster4;
    }

    private void UnlockMonster2()
    {
        _monster2Cover.enabled = false;
        _monster2Text.enabled = true;
    }

    private void UnlockMonster3()
    {
        _monster3Cover.enabled = false;
        _monster3Text.enabled = true;
    }

    private void UnlockMonster4()
    {
        _monster4Cover.enabled = false;
        _monster4Text.enabled = true;
    }
}
