﻿using UnityEngine;
 using UnityEngine.UI;
 using UnityEngine.Sprites;
 using System.Collections;
//sebastiaan
/// <summary>
/// dits script regelt alle slides van de tutorial
/// je kan hier simpel met buttons doorheen gaan.
/// </summary>

public class TutorialController : MonoBehaviour {
 [SerializeField]
 private Sprite[] _gallery;
 [SerializeField]
 private Image _displayImage; 
 [SerializeField]
 private Button _nextImg; 
 [SerializeField]
 private Button _prevImg; 
 private int i = 0; //Will control where in the array you are
 [SerializeField]
 private GameObject _button;
 [SerializeField]
 private GameObject _tutorial;
 [SerializeField]
 private  PrefTutorial _preft;
  [SerializeField]
 private  GameObject _ActivateTutButton;


 
   public void BtnNext (){
          if(i + 1 < _gallery.Length){
              i++;
          }
      }
     
   public void BtnPrev () {
          if (i > 0){
              i --;
          }
      }
 
   private void Update () {
    _displayImage.sprite = _gallery[i];
        if(i==5){
    _button.SetActive(true);
   
    
    }
  }
  public void DisableTutorial(){
      _tutorial.SetActive(false);
      _preft.tutiefrutie=1;
      _preft.SaveData();
      _ActivateTutButton.SetActive(true);
  }
   public void Reset(){
      _tutorial.SetActive(true);
      _preft.tutiefrutie=0;
      _preft.SaveData();
      _ActivateTutButton.SetActive(false);
  }
 }
 