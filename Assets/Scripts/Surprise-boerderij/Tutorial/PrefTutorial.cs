﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sebastiaan
/// <summary>
/// dit script kijkt of je de tutorial al eens eerder hebt gedaan zodat je dit niet nog een keer hoeft te dooen.
/// </summary>

public class PrefTutorial : MonoBehaviour
{
    [SerializeField]
    private int _tutSeen = 0;
    [SerializeField]
    private GameObject _tut;
    
    public int tutiefrutie { get { return _tutSeen; } set { _tutSeen = value; } }
    
     private void Awake()
    {
    
      GetData();
    }
    void Update(){

    if(_tutSeen==1){
        _tut.SetActive(false);
          }
    }

    private void GetData()
    {
       _tutSeen = PlayerPrefs.GetInt("tutorial");
     
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt("tutorial", _tutSeen);
     
    }
}