﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Tyrone

public class ResourceUI : MonoBehaviour
{
    [SerializeField]
    private Text _waterText;
    [SerializeField]
    private Text _foodText;
    [SerializeField]
    private Text _starpointText;

    private void Awake()
    {
        EventManager.onResourceUpdate += UiUpdate;
    }

    private void OnDestroy()
    {
        EventManager.onResourceUpdate -= UiUpdate;
    }

    private void UiUpdate()
    {
        _waterText.text = ResourceData.Instance.water.ToString();
        _foodText.text = ResourceData.Instance.food.ToString();
        _starpointText.text = ResourceData.Instance.starPoints.ToString();
    }
}
    
