﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sebastiaan

public class ResetTimerFood : MonoBehaviour
{
[SerializeField]
private FoodTimer _timerfood;
[SerializeField]
private ParticleSystem _particles;

    void OnMouseDown()
    {
        ResourceData.Instance.food += 25;
        _timerfood.timerFood = 10;
        EventManager.Instance.ResourceUpdate();
        _particles.Play();
    }
}
