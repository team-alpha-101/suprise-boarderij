﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//sebastiaan
/// <summary>
/// Dit Script zogt voor de timer van het eten.
/// </summary>

public class FoodTimer : MonoBehaviour
{
 [SerializeField]
 private GameObject _button;
 [SerializeField]
 private Timer _timer;

public float timerFood;


 private void Start()
 { 
    timerFood = _timer.FoodTimer;
    EventManager.Instance.ResourceUpdate();
 }

 private void Update()
 {
 
    if (timerFood >= 0.0f)
    {
        timerFood -= Time.deltaTime;
        _button.gameObject.SetActive(false);
    }

    if (timerFood <= 0.0f)
    {  
        timerEnded();
    }
 }
private void timerEnded()
 {
    _button.gameObject.SetActive(true);
 }
}
