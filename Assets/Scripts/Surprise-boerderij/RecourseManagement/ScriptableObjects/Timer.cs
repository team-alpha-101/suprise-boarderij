﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//sebastiaan
[CreateAssetMenu(fileName = "Timers", menuName = "Timers")]
public class Timer : ScriptableObject 
{
[SerializeField]
private float _foodTimer = 10;
[SerializeField]
private float _waterTimer = 10;
[SerializeField]
private float _flowerTimer = 10;
[SerializeField]
private float _flowerLifeTime = 60;

public float FoodTimer{
    get{return _foodTimer;}
    set{_foodTimer= value;}
}
public float WaterTimer{
    get{return _waterTimer;}
    set{_waterTimer= value;}
 }
public float flowerTimer{
    get{return _flowerTimer;}
    set{_flowerTimer= value;}
}

public float flowerLifeTime{
    get{return _flowerLifeTime;}
    set{_flowerLifeTime= value;}
}


}
