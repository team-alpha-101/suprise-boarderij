﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//sebastiaan

[CreateAssetMenu(fileName = "Storage", menuName = "Storage")]
public class PlayerStorage : ScriptableObject 
{
[SerializeField]
private int _food;
[SerializeField]
private int _stars;
[SerializeField]
private int _water;
public int Food{
    get{return _food;}
    set{ _food = value;}
}
public int Stars{
    get{return _stars;}
    set{_stars=value;}
}
public int Water{
    get{return _water;}
    set{_water=value;}
 }
}
