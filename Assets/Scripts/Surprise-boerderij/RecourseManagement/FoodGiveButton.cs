﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodGiveButton : MonoBehaviour
{
    [SerializeField]
    private MonsterType.monsterType monsterType;

    void OnMouseDown()
    {
        EventManager.Instance.FeedMonster(monsterType);
        EventManager.Instance.ResourceUpdate();      
    }
}
