﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone

public class WaterCollectButton : MonoBehaviour
{
    [SerializeField]
    private WaterTimer Timerwater;
    [SerializeField]
    private ParticleSystem particles;

    [SerializeField]
    private GameObject _waterCollectButton;

    [SerializeField]
    private int _waterAmount = 25;

    public int waterAmount { get { return _waterAmount; } set { _waterAmount = value; } }


    
    void OnMouseDown()
    {
        if (ResourceData.Instance.water < ResourceData.Instance.maxWater)
        {
            ResourceData.Instance.water += _waterAmount;
            Timerwater.TimerWater = 10;
            EventManager.Instance.ResourceUpdate();
            particles.Play();
        }
        else
        {
            _waterCollectButton.SetActive(false);
        }
    }

}