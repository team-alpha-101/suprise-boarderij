﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//sebastiaan
public class ResetTimerWater : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _particles;
    

    [SerializeField]
    private WaterTimer Timerwat;

    void OnMouseDown()
    {
        ResourceData.Instance.water += 25;
        Timerwat.TimerWater = 10;
        EventManager.Instance.ResourceUpdate();
        _particles.Play();
    }
   
}
