﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone
/// <summary>
/// dit script bevat alle data van de recources
/// </summary>
public class ResourceData : MonoBehaviour
{
    [SerializeField]
    private int _food;
    [SerializeField]
    private int _water;
    [SerializeField]
    private int _starPoints;

    [SerializeField]
    private int _maxWater = 100;
    [SerializeField]
    private int _maxFood = 100;

    public int food { get { return _food; } set { _food = value; } }
    public int water { get { return _water; } set { _water = value; } }
    public int starPoints { get { return _starPoints; } set { _starPoints = value; } }

    public int maxWater { get { return _maxWater; } set { _maxWater = value;}}
    public int maxFood { get { return _maxFood; } set { _maxFood = value; }}

    public static ResourceData Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }       
        GetData();
    }

    private void Update()
    {
        if (_food > maxFood)
        {
            _food = maxFood;
            EventManager.Instance.ResourceUpdate();
        }

        if (_water > maxWater)
        {
            _water = maxWater;
            EventManager.Instance.ResourceUpdate();
        }
    }

    private void GetData()
    {
       food = PlayerPrefs.GetInt("Food");
       water = PlayerPrefs.GetInt("Water");
       starPoints = PlayerPrefs.GetInt("StarPoints");
       maxWater = PlayerPrefs.GetInt("MaxWater");
       maxFood = PlayerPrefs.GetInt("MaxFood");
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt("Food", _food);
        PlayerPrefs.SetInt("Water", _water);
        PlayerPrefs.SetInt("StarPoints", _starPoints);
        PlayerPrefs.SetInt("MaxWater", _maxWater);
        PlayerPrefs.SetInt("MaxFood", _maxFood);
    }
}
