﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//sebastiaan
/// <summary>
/// Dit script zorgt voor de timer voor het spawnen van water.
/// </summary>

public class WaterTimer : MonoBehaviour
{
[SerializeField]
private GameObject _button;
[SerializeField]
private Timer _timer;

public float TimerWater;

 public void Start()
 {
     TimerWater =_timer.WaterTimer;
 }
 public void Update()
 {
    
     if (TimerWater >= 0.0f)
     {
         TimerWater -= Time.deltaTime;
         _button.gameObject.SetActive(false);
     }
 
     if (TimerWater <= 0.0f)
     {
        timerEnded();
     }
}
 private void timerEnded()
 {
    _button.gameObject.SetActive(true);
 }
 
}
