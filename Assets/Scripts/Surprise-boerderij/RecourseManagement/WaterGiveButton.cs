﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGiveButton : MonoBehaviour
{
    [SerializeField]
    private MonsterType.monsterType _monsterType;

    void OnMouseDown()
    {
        EventManager.Instance.GiveWater(_monsterType);
        EventManager.Instance.ResourceUpdate();
    }
}
