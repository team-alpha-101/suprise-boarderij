﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Tyrone

public class FoodCollectButton : MonoBehaviour
{
    [SerializeField]
    private FoodTimer _timerfood;
    [SerializeField]
    private ParticleSystem particles;

    [SerializeField]
    private GameObject _foodCollectButton;

    [SerializeField]
    private int _foodAmount = 25;

    public int foodAmount { get { return _foodAmount; } set { _foodAmount = value; } }


    void OnMouseDown()
    {
        if (ResourceData.Instance.food < ResourceData.Instance.maxFood)
        {
            ResourceData.Instance.food += _foodAmount;
            _timerfood.timerFood = 10;
            EventManager.Instance.ResourceUpdate();
            particles.Play();
        }
        else
        {
            _foodCollectButton.SetActive(false);
        }
    }
}